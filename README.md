# 项目说明

这是一个**合宙air32f103**的`arm-none-eabi-gcc`工具链的编译工程;

编译命令:

	make	顺序编译

	make -j	并发全速编译

	make clean	清除编译文件

支持OPENOCD下载调试，task.json中已经有openocd的下载命令，luanch.json中有调试的配置文件；
但是需要自己进行修改；
在cortex-debug插件的设置中修改必要的路径;

代码风格延续ST官方的HAL库代码风格--包括文件层次划分的习惯

由于OPENOCD里似乎没有AIR32的调试文件,所以借用了STM32F1的 .cfg 文件

仓库内已经防止了AIR32的svd文件所以可以查看内部寄存器等;

同时也支持反汇编功能;