/*
 * @Author: liuxcfreddy liuxcfreddy@163.com
 * @Date: 2024-01-30 16:45:03
 * @LastEditors: liuxcfreddy liuxcfreddy@163.com
 * @LastEditTime: 2024-01-30 16:55:16
 * @FilePath: \air32_stand\Core\Inc\usart.h
 * @Description: 
 * 
 * Copyright (c) 2024 by liuxcfreddy, All Rights Reserved. 
 */
#ifndef _USART_H_
#define _USART_H_

#include "main.h"
void UART_Configuration(void);

#endif