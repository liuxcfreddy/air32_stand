/*
 * @Author: liuxcfreddy liuxcfreddy@163.com
 * @Date: 2024-01-30 17:29:34
 * @LastEditors: liuxcfreddy liuxcfreddy@163.com
 * @LastEditTime: 2024-01-30 17:29:41
 * @FilePath: \air32_stand\Core\Inc\gpio.h
 * @Description: 
 * 
 * Copyright (c) 2024 by liuxcfreddy, All Rights Reserved. 
 */
#ifndef __GPIO_H_
#define __GPIO_H_

#include "main.h"

void MX_GPIO_Init(void);

#endif