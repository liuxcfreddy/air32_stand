/*
 * @Author: liuxcfreddy liuxcfreddy@163.com
 * @Date: 2024-01-30 17:29:28
 * @LastEditors: liuxcfreddy liuxcfreddy@163.com
 * @LastEditTime: 2024-01-30 17:46:00
 * @FilePath: \air32_stand\Core\Src\gpio.c
 * @Description: 
 * 
 * Copyright (c) 2024 by liuxcfreddy, All Rights Reserved. 
 */

#include "gpio.h"

/**
 * @brief GPIO初始化函数
 * @param  none
 */
void MX_GPIO_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	GPIO_InitTypeDef GPIO_InitStruct={0};
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_13;
	GPIO_Init(GPIOC,&GPIO_InitStruct);

}