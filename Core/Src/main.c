#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "main.h"
#include "usart.h"
#include "gpio.h"

void RCC_ClkConfiguration(void);

int main(void)
{
	RCC_ClkConfiguration();		//配置时钟
	Delay_Init();				//延时初始化
	UART_Configuration(); //串口初始化
	MX_GPIO_Init();
	while (1)
	{
		GPIO_WriteBit(GPIOC,GPIO_Pin_13,RESET);
		Delay_Ms(1000);
		printf("LED CLOSE\n");
		GPIO_WriteBit(GPIOC,GPIO_Pin_13,SET);
		Delay_Ms(1000);
		printf("LED OPEN\n");
	}
}

void RCC_ClkConfiguration(void)
{
	RCC_DeInit(); //复位RCC寄存器

	RCC_HSEConfig(RCC_HSE_ON); //使能HSE
	while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET)
		; //等待HSE就绪

	RCC_PLLCmd(DISABLE);										 //关闭PLL
	AIR_RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_27, FLASH_Div_2); //配置PLL,8*27=216MHz

	RCC_PLLCmd(ENABLE); //使能PLL
	while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
		; //等待PLL就绪

	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK); //选择PLL作为系统时钟

	RCC_HCLKConfig(RCC_SYSCLK_Div1); //配置AHB时钟
	RCC_PCLK1Config(RCC_HCLK_Div2);	 //配置APB1时钟
	RCC_PCLK2Config(RCC_HCLK_Div1);	 //配置APB2时钟

	RCC_LSICmd(ENABLE); //使能内部低速时钟
	while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
		;				//等待LSI就绪
	RCC_HSICmd(ENABLE); //使能内部高速时钟
	while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET)
		; //等待HSI就绪
}


/**
 * @description: 串口重定向部分
 * @param {int} ch
 * @return {*}
 */
int SER_PutChar(int ch)
{
	while (!USART_GetFlagStatus(USART1, USART_FLAG_TC))
		;
	USART_SendData(USART1, (uint8_t)ch);

	return ch;
}

int _write(int fd, char *ptr, int len)  
{  
	for (int i = 0; i < len; i++)
	{
		SER_PutChar(*ptr++);
	}
	return len;
}