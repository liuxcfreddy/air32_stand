/*
 * @Author: liuxcfreddy liuxcfreddy@163.com
 * @Date: 2024-01-30 16:44:54
 * @LastEditors: liuxcfreddy liuxcfreddy@163.com
 * @LastEditTime: 2024-01-30 17:09:51
 * @FilePath: \air32_stand\Core\Src\usart.c
 * @Description: 
 * 
 * Copyright (c) 2024 by liuxcfreddy, All Rights Reserved. 
 */

#include "usart.h"

USART_TypeDef *huart1=USART1;

void UART_Configuration(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(huart1, &USART_InitStructure);
	USART_Cmd(huart1, ENABLE);
}